# www-project
You can show project on gitlab hosting:<br>
https://rasal.gitlab.io/www-project/

___
Site description:
- Page is created in HTML5 and CSS3 technologies.

- Contact page contain Google Maps API and Email send form (disabled).
- Social media icons glow on the colors associated with these sites when you hover on them.
- In header you can see city wallpaper which moves with the movement of the scrolling page.
- Navigation menu contain few blocks which spreads up and all of them are glow when hover. 
- Website contain scroll back to the top button.
___

